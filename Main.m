% *********************************************************************************
% SIMPLE CODE FOR COMPUTING BETA RESONANT AND BETA CRITICAL
% Author: Marco Redolfi (marco.redolfi@unitn.it)
% Open Source code, distributed under GNU General Public Licence (GPLv3)
% NB:
% 	*This code is meant for research purposes only
%   *Not fully tested: please report possible issues at marco.redolfi@unitn.it
%   *This code uses default parameters:
%       Ikeda parameter:                        r=0.5 
%       transport formula:                      transport_f='Parker'
%       resistance formula:                     resistance_f='Engelund&Hansen'
%       relative submerged density of sediment: Delta=1.65
%       ->for setting different parameters see function documentation
% *********************************************************************************

close all
clear all
clc

%% Input parameters

Q=1000;      %Water discharge [m^3/s]
slope=0.001; %Channel gradient [m/m]
d50=0.02;    %Representative grain size [m]
W=100;       %Channel width [m]


%% Calculations

%Calculating reference depth by inverting the uniform flow relation
fun=@(x)(uniflow(x,d50,W,slope)-Q);
D0=fsolve(fun,.1);

%Calculating dimensionless reference parameters
Delta=1.65;                  %Relative sumberged density of sediment [-]
theta0=slope*D0/(Delta*d50); %Shields stress [-]
ds0=d50/D0;                  %Relative grain size [-]

%Computing resonant and critical aspect ratio
beta_R=beta_res_fct(theta0,ds0);  %Resonant aspect ratio [-]
beta_C=beta_crit_fct(theta0,ds0); %Critical aspect ratio for bar formation [-]

beta0=W/(2*D0); %Channel aspect ratio


%% Printing outputs

%Reference flow parameters
disp(sprintf('Water depth: %.3f m'      ,D0));
disp(sprintf('Aspect ratio: %.3f'       ,beta0));
disp(sprintf('Shields stress: %.3f'     ,theta0));
disp(sprintf('Relative grain size: %.3f',ds0));

%Resonant and critical values
disp(sprintf('Resonant aspect ratio: %.3f' ,beta_R));
disp(sprintf('Critical aspect aspect ratio for bar formation: %.3f' ,beta_C));
