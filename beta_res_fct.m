% ************************************************************************************
% EASY-TO-USE FUNCTION FOR COMPUTING RESONANT BETA
% Author: Marco Redolfi (marco.redolfi@unitn.it)
% Open Source code, distributed under GNU General Public Licence (GPLv3)
% This code is meant for research purposes, and has not been fully tested yet
% Please refer to the paper: Redolfi et al. (2019). https://doi.org/10.1002/esp.4561
% ************************************************************************************


function [beta_R,wavenumber_R]=beta_res_fct(theta0,ds0,varargin)

% OUTPUTS
% beta_R:       resonant aspect ratio [-]
% wavenumber_R: resonant wavenumber [-]

% INPUTS
% theta0: Shields stress [-]
% ds0:    relative grain size [-]

% OPTIONAL PARAMETERS
% r:               Ikeda parameter for gravitational effect on bed load (default=0.5)
% transport_f:     transport formula [string] (default='Parker')
%      current options: 'MP&M'; 'Wong&Parker'; 'Parker'; 'Parker_1978'; 'Engelund&Hansen'; 'Van_Rijn'
% resistance_f:    resistance formula [string] (default='Engelund&Hansen')
%      current options: 'Engelund&Hansen'; 'Engelund&Hansen_dunes'; 'Strickler'; 'Wilkerson&Parker'
% c0:              user-provided value of dimensionless Chèzy coefficient
% cD:              user-provided value of the coefficient cD
% cT:              user-provided value of the coefficient cT
% Delta:           relative submerged density of sediment (default=1.65)
% model:           'ZS4' (default) -> 4th-order model (Zolezzi and Seminara, 2001); 
%                  'ZS2'-> simplified 2nd-order model (Camporeale et al., 2007)

% EXAMPLES OF USAGE
% beta_R=beta_res_fct(theta0,ds0)
% [beta_R,wavenumber_R]=beta_res_fct(theta0,ds0)
% beta_R=beta_res_fct(theta0,ds0,'r',0.5)
% beta_R=beta_res_fct(theta0,ds0,'Delta',1.65,'transport_f','MP&M')

%*********************************************************************

%Parsing optional inputs
p = inputParser;
addParameter(p,'r'              ,0.5              ,@isnumeric);
addParameter(p,'transport_f'    ,'Parker'         ,@ischar);
addParameter(p,'resistance_f'   ,'Engelund&Hansen',@ischar);
addParameter(p,'c0'             ,[]               ,@isnumeric);
addParameter(p,'cD'             ,[]               ,@isnumeric);
addParameter(p,'cT'             ,[]               ,@isnumeric);
addParameter(p,'Delta'          ,1.65             ,@isnumeric);
addParameter(p,'model'          ,'ZS4'            ,@ischar);
parse(p,varargin{:});

%Assigning value to optional inputs
r              =p.Results.r;
transport_f    =p.Results.transport_f;
resistance_f   =p.Results.resistance_f;
c0_user        =p.Results.c0;
cD_user        =p.Results.cD;
cT_user        =p.Results.cT;
Delta          =p.Results.Delta;
model          =p.Results.model;


%% Definition of coefficients

[c0,cD,cT]=cderi(ds0,theta0,resistance_f,c0_user,cD_user,cT_user);
[PhiD,PhiT]=phideri(theta0,cD,cT,transport_f);


%% Calculation of resonant point

if strcmpi(model,'ZS2') %Second-order model (Camporeale et al., 2007)

    [beta_R,wavenumber_R]=beta_res_ZS2(theta0,r,c0,cD,cT,PhiD,PhiT);

elseif strcmpi(model,'ZS4') %Fourth-order model (Zolezzi and Seminara, 2001)

    %Seeking imaginary (no spatial damping/growing) value of lambda (lambda=wavenumber*i) for which beta is real
    
    %Imaginary part of beta as a function of wavenumber
    f=@(wavenumber) imag(beta_wavenumber(wavenumber,theta0,ds0,r,Delta,c0,cD,cT,PhiD,PhiT));  

    %Initial wavenumber from the simplified ZS2 model
    [~,wavenumber_init]=beta_res_ZS2(theta0,r,c0,cD,cT,PhiD,PhiT); 
 
    %Numerical solution
    options=optimset('Display','off'); %Setting solver options
    wavenumber_R=fsolve(f,wavenumber_init,options);

    %The resonant beta is the value corresponing to wavenumber_R
    beta_R=real(beta_wavenumber(wavenumber_R,theta0,ds0,r,Delta,c0,cD,cT,PhiD,PhiT));

else

    error('Unknown model!')   

end

return


%% Function that computes the resonant point according to the simplified model

function [beta_R,wavenumber_R]=beta_res_ZS2(theta0,r,c0,cD,cT,PhiD,PhiT);

    %Definition of useful coefficients (see Camporeale et al., 2007)
    f1=2/(1+2*cT);
    f2=-2*cD/(1+2*cT);
    P1=f1*PhiT;
    P2=f2*PhiT+PhiD;
    
    beta_R=pi/2*c0*sqrt(r)/theta0^(1/4)*sqrt(1/( (f2-1)*(1-P1)+f1*(P2-1) ));
    wavenumber_R=pi/2*sqrt(r)/(c0*theta0^(1/4))*sqrt(f1/(1-P2));

return


%% Function that gives growth rate as a function of the wavenumber

function beta=beta_wavenumber(wavenumber,theta0,ds0,r,Delta,c0,cD,cT,PhiD,PhiT);

lambda=wavenumber*i; %Complex wavelength

%Definition of useful coefficients (see Camporeale et al., 2007)
f1=2/(1+2*cT);
f2=-2*cD/(1+2*cT);
P1=f1*PhiT;
P2=f2*PhiT+PhiD;

k1=pi^2*r/sqrt(theta0)/4;
Fr0=sqrt(ds0*Delta*theta0*c0^2); %Froude number

%The system of linear equations can be conveniently written as:
%M=[
%[ A1      , A5      , A9         , 0        ] %Continuity
%[ A2+beta , 0       , A10*beta   , A14      ] %X-Momentum
%[ 0       , A7+beta , 0          , A15      ] %Y-Momentum
%[ A4      , A8      , A12+1/beta , A16/beta ] %Exner
%];
%where the Aj coefficients are given by the following matrix, with j indicating the linear index:
A=[
[ lambda         , -pi/2       , lambda        , 0              ]
[ lambda*c0^2/f1 , 0           , (f2-1)/f1     , lambda*c0^2/f1 ]
[ 0              , lambda*c0^2 , 0             , pi/2*c0^2      ]
[ -P1*lambda/k1  , pi/2/k1     , -lambda*P2/k1 , -Fr0^2         ]
];

%The determinant of the matrix M is given by
%det(M)*beta=
% (A9*A16 -A1*A10*A16 +A1*A12*A14 -A4*A9*A14 +A5*A12*A15 -A8*A9*A15 +A1*A8*A10*A15 -A4*A5*A10*A15)*beta^2 +
% (A1*A14 +A5*A15 +A2*A9*A16 +A7*A9*A16 -A1*A7*A10*A16 +A1*A7*A12*A14 +A2*A5*A12*A15 -A2*A8*A9*A15 - A4*A7*A9*A14)*beta +
% (A1*A7*A14 +A2*A5*A15 +A2*A7*A9*A16)

%The condition det(M)=0 can be therefore expressed as: a*beta^2+b*beta+c=0;
a=A(9)*A(16) -A(1)*A(10)*A(16) +A(1)*A(12)*A(14) -A(4)*A(9)*A(14) +A(5)*A(12)*A(15) -A(8)*A(9)*A(15) +A(1)*A(8)*A(10)*A(15) -A(4)*A(5)*A(10)*A(15);
b=A(1)*A(14) +A(5)*A(15) +A(2)*A(9)*A(16) +A(7)*A(9)*A(16) -A(1)*A(7)*A(10)*A(16) +A(1)*A(7)*A(12)*A(14) +A(2)*A(5)*A(12)*A(15) -A(2)*A(8)*A(9)*A(15) -A(4)*A(7)*A(9)*A(14);
c=A(1)*A(7)*A(14) + A(2)*A(5)*A(15) + A(2)*A(7)*A(9)*A(16);

%Solutions for beta: same imaginary part, reversed real part
%beta1=(-b+sqrt(b^2-4*a*c))/(2*a)
%beta2=(-b-sqrt(b^2-4*a*c))/(2*a)

%Take the solution with positive real part
beta=(-b+sqrt(b^2-4*a*c))/(2*a);
beta=abs(real(beta))+i*imag(beta);

return


%% Function that gives c0,cD,cT coefficients depending on the resistance formula
function [c0,cD,cT]=cderi(ds0,theta0,resistance_f,c0,cD,cT)

switch lower(resistance_f)
    
    case lower('Engelund&Hansen') %Engelund and Hansen (1967) formula (flat bed)

        if isempty(c0)
            c0=6+2.5*log(1/(2.5*ds0));
        end

        if isempty(cD); cD=2.5/c0; end
        if isempty(cT); cT=0;      end

    case lower('Engelund&Hansen_dunes') %Engelund and Hansen (1967) formula (dunes)

        f0=(0.06+0.4*theta0^2)/theta0; 
        
        if isempty(c0)
            c0=sqrt(f0)*(6+2.5*log(f0/(2.5*ds0)));
        end

        if isempty(cD); cD=2.5*sqrt(f0)/c0; end
        if isempty(cT)
            fT=theta0/f0*(-0.06/(theta0^2)+0.4); %Relative variation of f with theta
            cT=(1/2+2.5*sqrt(f0)/c0)*fT; 
        end

    case lower('Strickler') %Strickler (1923) formula

        if isempty(c0)
            c0=21.1/(9.81^0.5)/ds0^(1/6);
        end
        if isempty(cD); cD=1/6; end
        if isempty(cT); cT=0;   end

    case lower('Wilkerson&Parker') %Wilkerson and Parker (2011) formula

        if isempty(c0)
            c0=4.88/ds0^0.083;
        end
        if isempty(cD); cD=0.083; end
        if isempty(cT); cT=0;     end

    otherwise

        error('Error: Unknown resistance formula!')

end

return


%% Function that givers PhiD and PhiT coefficients depending on the transport formula

function [PhiD,PhiT,theta_cr]=phideri(theta0,cD,cT,transport_f)

switch lower(transport_f) 

    case lower('MP&M') %Meyer-Peter and Muller (1948) formula
        
        theta_cr=0.047; %Critical Shields stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end

        %Phi0=8*(theta0-theta_cr)^1.5;
        PhiT=1.5*theta0/(theta0-theta_cr);
        PhiD=0;

    case lower('Wong&Parker') %Wong and Parker (1996)

        theta_cr=0.047; %Critical Shield stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end

        %Phi0=4.93.*(theta0-theta_cr)^1.6;
        PhiT=1.6*theta0/(theta0-theta_cr);
        PhiD=0;

    case lower('Parker_1978') %Parker (1978)

        theta_cr=0.03; %Critical Shields stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end

        %Phi0=11.2*theta0^1.5*(1-theta_cr/theta0)^4.5;
        PhiT=1.5+4.5*theta_cr/(theta0-theta_cr);
        PhiD=0;

    case lower('Parker') %Parker (1990)

        theta_cr=0; %Critical Shields stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end    

        %Parameters
        A=0.0386;
        B=0.853;
        C=5474;
        D=0.00218;

        x=theta0/A; %Normalized Shields stress in Parker formula
        if x>1.59
            Phi0=C*D*theta0^1.5*(1-B/x)^4.5;
            Phi_der=1.5*theta0^0.5*(1-B/x)^4.5*C*D+4.5*A*B*(1-B/x)^3.5*C*D/(theta0^0.5);
        elseif x>1
            Phi0=D*(theta0^1.5)*(exp(14.2*(x-1)-9.28*(x-1)^2));
            Phi_der=1/A*Phi0*(14.2-9.28*2*(x-1)) + 1.5*Phi0/theta0;
        else
            Phi0=D*theta0^1.5*x^14.2;
            Phi_der=14.2/A*D*theta0^1.5*x^13.2 + D*x^14.2*1.5*theta0^0.5;
        end

        PhiT=theta0/Phi0*Phi_der;
        PhiD=0;

    case lower('Engelund&Hansen') %Engelund and Hansen (1967)

        theta_cr=0; %Critical Shields stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end    

        %alpha_EH=1;
        %Phi0=0.05*alpha_EH*c0^2*(theta0)^2.5;
        PhiT=2.5+2*cT;
        PhiD=2*cD;    

    case lower('Van_Rijn') %Van Rijn (1984)  

        theta_cr=0.055; %Critical Shields stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end
            
        if theta0/(theta0-theta_cr)<3.0
            PhiT=2.1*theta0/(theta0-theta_cr);
        else
            PhiT=1.5*theta0/(theta0-theta_cr);
        end    
        PhiD=0;

    otherwise

        error('Unknown transport formula!')

end

return
